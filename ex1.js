//1. Implemente um algoritmo que pegue duas matrizes (array de arrays) e
//realize sua multiplicação. Lembrando que para realizar a multiplicação
//dessas matrizes o número de colunas da primeira matriz tem que ser
//igual ao número de linhas da segunda matriz. (2x2)
//a. Caso teste 1 : [ [ [2],[-1] ], [ [2],[0] ] ] e [ [2,3],[-2,1] ] multiplicadas dão
//[ [6,5], [4,6] ]
//b. Caso teste 2 : [ [4,0], [-1,-1] ] e [ [-1,3], [2,7] ] multiplicadas dão [
//[-4,12], [-1,-10] ]



function multiplicarMatriz(matrizA, matrizB) {
    let linhasA = matrizA.length;
    let colunasA = matrizA[0].length;
    let linhasB = matrizB.length;
    let colunasB = matrizB[0].length;
    let matrizC = [];

    if (colunasA != linhasB) {
        return false;
    }

    for (let i = 0; i < linhasA; i++) {
        matrizC[i] = [];
    }

    for (let k = 0; k < colunasB; k++) {
        for (let i = 0; i < linhasA; i++) {
            let total = 0;
            for (let j = 0; j < linhasB; j++) {
                total += matrizA[i][j] * matrizB[j][k];
            }
            matrizC[i][k] = total;
        }
    }

    return matrizC;
}


console.log(multiplicarMatriz([[[2],[-1]],[[2],[0]]],[[2,3],[-2,1]]))