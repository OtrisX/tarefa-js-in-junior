//3. Faça um algoritmo em que você recebe 3 notas de um aluno e caso a
//média aritmética dessas notas for maior ou igual que 6 imprima
//“Aprovado”, caso contrário “Reprovado”.

function aprovar(n1,n2,n3) {
    return ((n1 + n2 + n3) / 3 >= 6) ? "Aprovado" : "Reprovado"
}


console.log(aprovar(6,6,6))