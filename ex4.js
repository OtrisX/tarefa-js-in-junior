//4. Vocês receberão um arquivo com um array de objetos representando
//deuses do jogo Smite. Usando os métodos aprendidos em aula, faça os
//seguintes exercícios:
//● Q1. Imprima o nome e a quantidade de features de todos os deuses usando
//uma única linha de código.
//● Q2. Imprima todos os deuses que possuem o papel de "Mid"
//● Q3. Organize a lista pelo panteão do deus.
//● Q4. Faça um código que retorne um novo array com o nome de cada deus e
//entre parênteses, a sua classe.

import gods from "./arquivo_exercicio_4.js"

function q1() {
    for (let god of gods){console.log(`Nome: ${god.name}\nFeatures: ${god.features.length}\n`)}
}

function q2() {
    for (let god of gods){
        if(god.roles.includes("Mid")) console.log(god)
    }
}

function q3() {
    let tempGod = gods
    
    console.log(tempGod.sort(function(a,b) {
        if (a.pantheon < b.pantheon) {
            return -1;
        }
        if (a.pantheon > b.pantheon) {
            return 1;
        }
        return 0;
    }))
}

function q4() {
    const res = []
    for (let god of gods){
        res.push(`${god.name} (${god.class})`)
    }
    console.log(res)
}


//q1()
//q2()
//q3()
q4()