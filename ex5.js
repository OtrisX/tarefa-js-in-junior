//5. Teste 5 números inteiros aleatórios. Os testes:
//● Caso o valor seja divisível por 3, imprima no console “fizz”.
//● Caso o valor seja divisível por 5 imprima “buzz”.
//● Caso o valor seja divisível por 3 e 5, ao mesmo tempo, imprima
//“fizzbuzz”.
//● Caso contrário imprima nada.


function ex5(x) {
    if(x % 3 == 0 && x % 5 == 0) return("fizzbuzz")
    if(x % 5 == 0) return("buzz")
    if(x % 3 == 0) return("fizz")
}



console.log(ex5(3))